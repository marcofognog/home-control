FROM ruby:2.7.0

WORKDIR /app

COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN gem install bundler
RUN bundle install

COPY . .

CMD ["sh" "-c", "echo Command missing; exit 1"]
