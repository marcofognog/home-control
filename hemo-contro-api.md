HOST: https://hemo-control.herokuapp.com

# HemoControl

Backend API fo help hemophilia patients to manage their medicine injections.

# Group Patient

## Patient [/patient/:id]

### Create [POST]

+ Relation: create

+ Request (application/json)
  + Body
      {
          "patient": {
              "name": "Markus Söder",
              "api_key": "23rj3892398j499j9an2"
          }
      }

+ Response 201 (application/json; charset=utf8)
  + Body
      {
        "id": 7,
        "name": "markus",
        "api_key": "r7K8sXvd5x1pCJGqrTiwPg",
        "created_at": "2021-08-22T08:08:53.423Z",
        "updated_at": "2021-08-22T08:08:53.423Z"
      }

# Group InjectionPlans

## InjectionPlans [/patient/:id/injection_plan]

### Create [POST]

+ Relation: create

+ Request (application/json)
  + Body
      {
          "injection_plan": {
             "name": "First treatment with something",
             "every_x_days": 3
          }
      }

+ Response 201 (application/json; charset=utf8)
  + Body
    {
      "id": 1,
      "name": "First treatment",
      "every_x_days": 3,
      "patient_id": 7,
      "created_at": "2021-08-22T08:14:00.825Z",
      "updated_at": "2021-08-22T08:14:00.825Z"
    }

# Group Injections

## Injections [/patient/:id/injections]

### Create [POST]

+ Relation: create

+ Request (application/json)
  + Body
      {
           "injection": {
               "applied_at": "2021-08-17 16:30:00",
               "dose": 200,
               "lot_number": 'abc123',
               "drug_name": 'desmopressin',
               "injection_plan_id": 23
           }
       }

+ Response 201 (application/json; charset=utf8)

  + Body
      {
           "injection": {
               "applied_at": "2021-08-17 16:30:00",
               "dose": 200,
               "lot_number": 'abc123',
               "drug_name": 'desmopressin',
               "injection_plan_id": 23
           }
       }

### Index [GET]

+ Request (application/json)

+ Response 200 (application/json; charset=utf8)
  + Body
     [
       {
         "id": 1,
         "dose": 200,
         "lot_number": "abc123",
         "drug_name": "desmopressin",
         "applied_at": "2021-08-15T16:30:00.000Z",
         "injection_plan_id": 1,
         "created_at": "2021-08-22T08:14:13.827Z",
         "updated_at": "2021-08-22T08:14:13.827Z"
       }
     ]

# Group Adherence

## Adherence [/patient/:id/adherence]

### Index [GET]

+ Request (application/json)

+ Response 200 (application/json; charset=utf8)
  + Body
      {
        "score": "73",
      }
