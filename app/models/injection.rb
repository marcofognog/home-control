class Injection < ApplicationRecord
  belongs_to :injection_plan

  validates :lot_number, presence: true, length: { is: 6 }
  validates :drug_name, presence: true
  validates :applied_at, presence: true
  validates :dose, presence: true

  # TODO: use specific field for start_at instead of created_at
  def on_time?
    return true if injection_plan.created_at.to_date == applied_at.to_date

    injection_order_number = injection_plan.injections.order(:created_at).all.find_index(self) # zero base index

    desired_day = injection_plan.created_at + (injection_order_number * injection_plan.every_x_days).days

    desired_day.day == applied_at.day
  end
end
