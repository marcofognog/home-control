class InjectionPlan < ApplicationRecord
  belongs_to :patient
  has_many :injections
end
