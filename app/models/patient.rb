class Patient < ApplicationRecord
  has_one :injection_plan

  validates :name, presence: true

  def adherence
    { score: adherence_score }
  end

  def adherence_score
    # TODO: use specific field for start_at instead of created_at
    days_since_plan_started = (Date.today.mjd - injection_plan.created_at.to_date.mjd)
    count_of_shot_days_until_today = (days_since_plan_started / injection_plan.every_x_days) + 1 # add first shot
    on_time_injections = injection_plan.injections.select(&:on_time?).count

    if on_time_injections.zero?
      0
    else
      ((on_time_injections.to_f / count_of_shot_days_until_today) * 100).round.to_i
    end
  end
end
