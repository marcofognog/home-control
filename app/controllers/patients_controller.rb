class PatientsController < ApplicationController
  def create
    # TODO: move to one layer bellow
    # TODO: encrypt this
    api_key = SecureRandom.urlsafe_base64

    @patient = Patient.new(patient_params.merge(api_key: api_key))

    if @patient.save
      render json: @patient, status: :created
    else
      render json: @patient.errors, status: :unprocessable_entity
    end
  end

  private

  def patient_params
    params.require(:patient).permit(:name)
  end
end
