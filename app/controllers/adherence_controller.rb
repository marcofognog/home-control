class AdherenceController < ApplicationController
  before_action :authenticate_patient_key

  def index
    patient = Patient.find(params[:patient_id])
    render json: patient.adherence
  end
end
