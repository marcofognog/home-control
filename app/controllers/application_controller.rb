class ApplicationController < ActionController::API
  def authenticate_patient_key
    patient = Patient.find(params[:patient_id])

    render status: 401, text: 'Unauthorized' unless patient.api_key == params[:api_key]
  end
end
