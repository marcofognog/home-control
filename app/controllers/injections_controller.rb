class InjectionsController < ApplicationController
  before_action :authenticate_patient_key

  def index
    patient = Patient.find(params[:patient_id])
    @injections = patient.injection_plan.injections

    render json: @injections
  end

  def create
    @injection = Injection.new(injection_params)

    if @injection.save
      render json: @injection, status: :created
    else
      render json: @injection.errors, status: :unprocessable_entity
    end
  end

  private

  def injection_params
    params.require(:injection).permit(:dose, :lot_number, :drug_name, :patient_id, :applied_at, :injection_plan_id)
  end
end
