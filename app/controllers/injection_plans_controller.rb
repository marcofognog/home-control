class InjectionPlansController < ApplicationController
  before_action :authenticate_patient_key

  def create
    @injection_plan = InjectionPlan.new(injection_plan_params.merge(patient_id: params[:patient_id]))

    if @injection_plan.save
      render json: @injection_plan, status: :created
    else
      render json: @injection_plan.errors, status: :unprocessable_entity
    end
  end

  private

  def injection_plan_params
    params.require(:injection_plan).permit(:name, :every_x_days)
  end
end
