Rails.application.routes.draw do
  resources :patients, only: [:create] do
    resources :injections, only: [:index, :create]

    resources :injection_plans, only: [:create]

    # Assuming 1 to 1 cardinality between injection_plan and patients
    resources :adherence, only: [:index]
  end

  get '/', to: redirect('api.html')
end
