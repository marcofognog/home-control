require 'rails_helper'

describe Injection do
  describe '#on_time?' do
    it 'tells if the current shot was taken in the correct day' do
      start_time = DateTime.now
      patient = Patient.create(name: 'Markus Söder')
      injection_plan = InjectionPlan.create(name: 'Moderate plan', every_x_days: 3, patient_id: patient.id)
      injection = Injection.create(
        applied_at: DateTime.now,
        dose: 200, lot_number: 'abc123',
        drug_name: 'desmopressin',
        injection_plan_id: injection_plan.id
      )

      second_shot_time = start_time + 3.days
      Timecop.travel(second_shot_time)

      second_injection = Injection.create(
        applied_at: DateTime.now,
        dose: 200, lot_number: 'abc123',
        drug_name: 'desmopressin',
        injection_plan_id: injection_plan.id
      )

      expect(injection.on_time?).to be true
      expect(second_injection.on_time?).to be true
    end
  end
end
