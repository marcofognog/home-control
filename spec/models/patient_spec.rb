require 'rails_helper'

# rubocop:disable Metrics/BlockLength
describe Patient do
  describe '#adherence' do
    it 'returns 100 for a single shor in the first day' do
      patient = Patient.create(name: 'Markus Söder')
      injection_plan = InjectionPlan.create(name: 'Moderate plan', every_x_days: 3, patient_id: patient.id)
      Injection.create(
        applied_at: DateTime.now,
        dose: 200, lot_number: 'abc123',
        drug_name: 'desmopressin',
        injection_plan_id: injection_plan.id
      )

      expect(patient.adherence_score).to eq(100)
    end

    it 'returns 100 when all shots were taken on time' do
      start_time = DateTime.now
      patient = Patient.create(name: 'Markus Söder')
      injection_plan = InjectionPlan.create(name: 'Moderate plan', every_x_days: 3, patient_id: patient.id)
      Injection.create(
        applied_at: DateTime.now,
        dose: 200, lot_number: 'abc123',
        drug_name: 'desmopressin',
        injection_plan_id: injection_plan.id
      )

      second_shot_time = start_time + 3.days
      Timecop.travel(second_shot_time)

      Injection.create(
        applied_at: DateTime.now,
        dose: 200, lot_number: 'abc123',
        drug_name: 'desmopressin',
        injection_plan_id: injection_plan.id
      )

      expect(patient.adherence_score).to eq(100)
    end

    # rubocop:disable Lint/UselessAssignment
    it 'returns 73 when 11 shots were taken but 3 were in the wrong days' do
      start_time = DateTime.now
      patient = Patient.create(name: 'Markus Söder')
      injection_plan = InjectionPlan.create(name: 'Moderate plan', every_x_days: 3, patient_id: patient.id)
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      second_shot_time = start_time + 3.days
      Timecop.travel(second_shot_time)
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      Timecop.travel(start_time + 5) # should have take in 6 days
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      forth_shot_time = start_time + 9
      Timecop.travel(start_time + 9)
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      fifth_shot_time = start_time + 12
      Timecop.travel(start_time + 12)
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      sixth_shot_time = start_time + 15
      Timecop.travel(start_time + 15)
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      seventh_shot_time = start_time + 18
      Timecop.travel(start_time + 18)
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      eighth_shot_time = start_time + 21
      Timecop.travel(start_time + 21)
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      Timecop.travel(start_time + 25) # should have taken in 24 days
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      Timecop.travel(start_time + 29) # should have taken in 27 days
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      eleventh_shot_time = start_time + 30
      Timecop.travel(start_time + 30)
      Injection.create(applied_at: DateTime.now, dose: 200, lot_number: 'abc123',
                       drug_name: 'desmopressin', injection_plan_id: injection_plan.id)

      expect(patient.adherence_score).to eq(73)
    end
  end
  # rubocop:enable Lint/UselessAssignment
  # rubocop:enable Metrics/BlockLength
end
