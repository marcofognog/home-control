require 'rails_helper'

RSpec.describe 'InjectionsControllers', type: :request do
  describe 'POST' do
    it 'creates new injection' do
      # TODO: Use FactoryBot here
      patient = Patient.create(name: 'Markus Söder', api_key: '123123')
      injection_plan = InjectionPlan.create(name: 'Moderate plan', every_x_days: 3, patient_id: patient.id)

      post "/patients/#{patient.id}/injections?api_key=#{patient.api_key}", params: {
        injection: {
          applied_at: DateTime.now,
          dose: 200, lot_number: 'abc123',
          drug_name: 'desmopressin',
          injection_plan_id: injection_plan.id
        }
      }

      expect(response).to have_http_status(201)
    end
  end

  describe 'GET /patient/:id/injections' do
    it 'retrives all injections for a patient' do
      patient = Patient.create(name: 'Markus Söder', api_key: '123123')
      injection_plan = InjectionPlan.create(name: 'Moderate plan', every_x_days: 3, patient_id: patient.id)
      Injection.create(
        applied_at: DateTime.now,
        dose: 200, lot_number: 'abc123',
        drug_name: 'desmopressin',
        injection_plan_id: injection_plan.id
      )

      get "/patients/#{patient.id}/injections?api_key=#{patient.api_key}"

      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).first['dose']).to eq(200)
      expect(JSON.parse(response.body).first['drug_name']).to eq('desmopressin')
    end

    it 'returns unauthorized if api_key is incorrect' do
      patient = Patient.create(name: 'Markus Söder', api_key: '123123')
      injection_plan = InjectionPlan.create(name: 'Moderate plan', every_x_days: 3, patient_id: patient.id)
      Injection.create(
        applied_at: DateTime.now,
        dose: 200, lot_number: 'abc123',
        drug_name: 'desmopressin',
        injection_plan_id: injection_plan.id
      )

      incorrect_api_key = '321321'
      get "/patients/#{patient.id}/injections?api_key=#{incorrect_api_key}"

      expect(response).to have_http_status(401)
    end
  end
end
