require 'rails_helper'

describe 'GET /patient/:id/adherence' do
  it 'returns unauthorized if api_key is incorrect' do
    patient = Patient.create(name: 'Markus Söder', api_key: '123123')
    injection_plan = InjectionPlan.create(name: 'Moderate plan', every_x_days: 3, patient_id: patient.id)
    Injection.create(
      applied_at: DateTime.now,
      dose: 200, lot_number: 'abc123',
      drug_name: 'desmopressin',
      injection_plan_id: injection_plan.id
    )

    incorrect_api_key = '321321'
    get "/patients/#{patient.id}/adherence?api_key=#{incorrect_api_key}"

    expect(response).to have_http_status(401)
  end

  it 'retrives adherence score for a patient' do
    start_time = DateTime.now
    patient = Patient.create(name: 'Markus Söder', api_key: '123123')
    injection_plan = InjectionPlan.create(name: 'Moderate plan', every_x_days: 3, patient_id: patient.id)
    Injection.create(
      applied_at: DateTime.now,
      dose: 200, lot_number: 'abc123',
      drug_name: 'desmopressin',
      injection_plan_id: injection_plan.id
    )

    second_shot_time = start_time + 3.days
    Timecop.travel(second_shot_time)

    Injection.create(
      applied_at: DateTime.now,
      dose: 200, lot_number: 'abc123',
      drug_name: 'desmopressin',
      injection_plan_id: injection_plan.id
    )

    get "/patients/#{patient.id}/adherence?api_key=#{patient.api_key}"

    expect(response).to have_http_status(200)
    expect(JSON.parse(response.body)['score']).to eq(100)
  end
end
