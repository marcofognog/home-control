require 'rails_helper'

RSpec.describe 'PatientsControllers', type: :request do
  describe 'POST' do
    it 'creates new patient' do
      post '/patients', params: {
        patient: {
          name: 'Markus Söder'
        }
      }

      expect(response).to have_http_status(201)
      expect(JSON.parse(response.body)['api_key']).to be_truthy
      expect(Patient.first.api_key).to be_truthy
    end
  end
end
