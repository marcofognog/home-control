require 'rails_helper'

RSpec.describe 'InjectionPlansController', type: :request do
  describe 'POST' do
    it 'creates new injection plan for patient' do
      patient = Patient.create(name: 'Markus Söder', api_key: '123123')
      post "/patients/#{patient.id}/injection_plans?api_key=123123", params: {
        injection_plan: {
          name: 'First treatment with something',
          every_x_days: 3
        }
      }

      expect(response).to have_http_status(201)
      expect(Patient.first.injection_plan.every_x_days).to eq(3)
    end
  end
end
