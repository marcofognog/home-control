class CreateInjectionPlans < ActiveRecord::Migration[6.0]
  def change
    create_table :injection_plans do |t|
      t.string :name
      t.integer :every_x_days
      t.integer :patient_id

      t.timestamps
    end
  end
end
