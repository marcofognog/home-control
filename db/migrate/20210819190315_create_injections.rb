class CreateInjections < ActiveRecord::Migration[6.0]
  def change
    create_table :injections do |t|
      t.integer :dose
      t.string :lot_number
      t.string :drug_name
      t.datetime :applied_at
      t.integer :injection_plan_id

      t.timestamps
    end
  end
end
