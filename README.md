# HemoControl API

Backend API fo help hemophilia patients to manage their medicine injections.

![challenge](problem.png)

## User story

A patient is created and assigned an ID and api_key:

`$ sample-calls/./create-patient.sh`

After consultation with the doctor, an injection plan is created (change parameters `id` and `api_key`, to the ones from the created patient):

`$ sample-calls/./create-injection-plan.sh`

The patient then decides to start right a way and take first injection of the plan: (change parameters `id`, `api_key` and `injection_plan_id`)

`$ sample-calls/./create-injection.sh`

Now the patient wants to see how it is going with the injections, we can list them: (change parameters `id`, `api_key`)

PS: You could wait a few days and create another to have more fun adherence score, if you are really commited to the manual test :)

`$ sample-calls/./list-injection.sh`

And finally, patient can see the adherence score;

`$ sample-calls/./get-adherence-score.sh`


## Instructions

To run API server locally:

`$ docker-compose up`

To run specs:

`$ docker-compose run app rspec`

Deployment:

Press button on the CI pipeline and repository is pushed to Heroku, migrations run right after deployment (sic).

## The InjectionPlan entity

The challenge instructions don't mention the entity `InjectionPlan` but I assumed it would be a appropriate because
I'm not sure one patient can only have one injection program at a time, but even if so is true, patient could swicth
from one plan to the other and we would have all the history.

Example:

Patient starts with shots every 5 days for a month,
but later they stop for 3 weeks and restart with another plan for shots every 7 days.

## Patient id and api_key

I know, it is redundant to pass both every call. I really don't think `api_key` should belong to patient but I left it there
because the instruction were very clear on that. The `api_key` would be a better fit in a `ClientApp`-like model.
Then we only use patient's id as reference.

Example:

I created a new Twiter app, and received an api_key that grants me access to their api, and has not relation to users.

## Showoff wishlist

It would have been nice if we could've seen here:

 * intermediate layer to hide logic from controllers
 * serializers (presenters)
 * wrapping external dependencies for injection
 * background jobs
 * More abstract entities implemented (perhaps the adherence score would get there, but I kept it to the instructions for simplicity's sake)
 * More infrastructure (terraform, Kubernets, etc...)

## API doc generation:

http://hemo-control.herokuapp.com/api.html

(Generation tool: https://github.com/danielgtaylor/aglio)

`aglio -i hemo-contro-api.md -o public/api.html`

## TODO's
 - [ ] Add api doc generation to CI
 - [ ] Adjust Heroku deployment to run migrations first on separate pod
 - [ ] Improve adherence score implementation (quite naive currently)
 - [ ] A frontend would be nice :)
